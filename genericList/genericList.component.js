'use strict';
// genericList component and its template and controller.
angular.
  module('genericList').
  component('genericList', {
    templateUrl: 'genericList/genericList.template.html',
    controller: ['GenericService', function genericListController(GenericService) {
      this.listItems = GenericService.getAll();
    }]
  });
