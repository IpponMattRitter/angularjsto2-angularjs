'use strict';

// Register `phoneDetail` component, along with its associated controller and template
angular.
  module('genericDetail').
  component('genericDetail', {
    templateUrl: 'genericDetail/genericDetail.template.html',
    controller: ['$routeParams','GenericService',
        function GenericDetailController($routeParams, GenericService) {
        var id = $routeParams.itemId;
        this.item = GenericService.getById(id);
      }
    ]
  });