'use strict';

angular.
  module('JSv2js').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/list', {
          template: '<generic-list></generic-list>'
        }).
        when('/item/:itemId', {
          template: '<generic-detail></generic-detail>'
        }).
        otherwise('/list');
    }
  ]);