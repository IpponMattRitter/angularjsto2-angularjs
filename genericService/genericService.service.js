angular.
  module('SharedServices').factory("GenericService", function(){

    var genericService = {};

    genericService.genericItems = [
        {
          id: '1',
          name: 'Aurelia',
          description: 'Aurelia is a genus of scyphozoan jellyfish, commonly called moon jellies. There are at least 13 species in the genus Aurelia including many that are still not formally described.'
        },
        {
          id: '2',
          name: 'Deepstaria',
          description: 'A genus of jellyfish known for their thin, sheet-like bodies.'
        },
        {
          id: '3',
          name: 'Stellamedusa ventana',
          description: 'The species was first described in the Journal of the Marine Biological Association in 2004 by Kevin Raskoff and George Matsumoto of the Monterey Bay Aquarium Research Institute.'
        },
        {
          id: '4',
          name: 'Stygiomedusa gigantea',
          description: 'The jellyfish has an umbrella-shaped bell that can be up to a meter wide. It also has four "paddle-like" arms up to 10 metres long.'
        },
    ];

    genericService.getAll = function(){
        return genericService.genericItems;
    }

    genericService.getById = function(id){
        for(var item in genericService.genericItems){
            if(genericService.genericItems[item].id === id) {
                return genericService.genericItems[item];
            }
        }
    };

    return genericService;

});
